# Demoing capability of Recurrent Neural Network using RNN-software for R-language

From book `Deep Learning with Python`:  
 >   In summary, you don’t need to understand everything about the specific architecture of an LSTM cell; as a human, it shouldn’t be your job to understand it. Just keep in mind what the LSTM cell is meant to do: allow past information to be reinjected at a later time.

This repository contains 3 files testing the capability of [rnn](https://cran.r-project.org/web/packages/rnn/) package for R-language. Additionally, in this repository there is one program which is an example of how to do rnn-learning using [Keras/Tensorflow](https://tensorflow.rstudio.com/) package for R-language.

### Practicing with RNN-package

This exercise file(`demo_rnn_tests.R`) is copied from [here](https://www.r-exercises.com/2017/06/21/neural-networks-exercises-part-3/) and gives a basic understanding of how does the rnn-package work.

### Basic example of how to use Keras

This example is straight copied form [here](https://www.kaggle.com/rtatman/beginner-s-intro-to-rnn-s-in-r).  
The file (`demo_keras.R`) and the website gives fairly good explanation of how to use RNN capabilites of the keras/tensorflow package. Tensorflow is currently one of the most used machine learning software. It works on multiple programming languages including R, Python, C++, Javascript and Java.  

Following the website and the comments on the file, one should get a basic understanding what is happening in the code.

### Converting Keras example to use RNN-package

Next step in learning how to use rnn-package was converting the previous well explained weather example to use rnn-package instead of keras.(`demo_rnn.R`) This was fairly straight forward, after understanding the data-form `trainr`-function wants;   
   
Data should be in a form where 1st dimension is samples(should be same size in X and Y), 2nd is time(should be same size in X and Y), and 3rd optional variables dimension.  

- samples = the number of observation. This is the number of sample you have in your dataset, same as for classical training with other algorithm in R
- time = is the length in time of each input (e.g. the number of words in a sentence).
- variable = the number of input/output unit.   
   
once more: "the first dimension is for the number of samples, the second one is for the number of elements in the sequence and the optional third dimension is for the number of variables"

And then the function using the data:   
  
```
trained_model <- trainr(
    Y = y_train, # sequence we're predicting
    X = X_train, # sequence we're using for prediction
    hidden_dim = c(6), #hidden layer dimensions
    network_type = "rnn", #other types are gru and lstm, they are experimental in this package.
    sigmoid = c("logistic"), #sigmoid function
    batch_size = batch_size, # how many samples to pass to our model at a time
    numepochs = total_epochs, # how many times we'll look @ the whole dataset
    learningrate = 0.1) # how much data to hold out for testing as we go along
```
After this we can predict the data using `predictr` function.  
We can compare results that we got using keras package and we can notice that prediction accuracy is similar ~70% with both packages.

### Own stock(SP500) prediction

Finally, there is a file(`demo_rnn_stocks.R`) that predicts SP500 using rnn-package. Most problems came from modifying the data to the form which `trainr`-function accepts from the rnn-package. More problems arose from normalizing the data in the smart way that it could be later de-normalized for plotting purpose. If we wouldn't normalize, rnn couldn't predict numbers that are out of the scale. A common way when predicting the stocks is to normalize data by using Z-Score Standardization in every splitted window. Other way would be Min-Max Normalization, which is also presented here.  
Anyways, the comments in the file should explain what happens in each step.

![SP500](./Sp500_prediction.png "SP500 Prediction")
![SP500 full](./SP500_Prediction_full.png "SP500 Prediction")


## Conclusion

I myself had to start learning R-language from the beginning, as I had not used it before. But after basic syntax and couple funny differences (compared to eg. python and c++) was understood, writing R was pleasant. The biggest problem came from understanding the [RNN-package](https://cran.r-project.org/web/packages/rnn/) as in my opinion, documentation is limited and feels like unfinished. Same for the examples or tutorials on how to use the package; there wasn't really any good basic ones that would explain usage of the functions, algorithms used or even features that this software contains. I had to read the source code and experiment a lot myself with how different functions and features work.  

I also quickly managed to test the [kohonen-package](https://cran.r-project.org/web/packages/kohonen/) which is, in my opinion, well documented. There is plenty of tutorials on how to use the package and examples exploring the different features of the software. I can publish my small tests if required.

What I didn't manage was to understand how I could either write an implementation of kohonen-neutron myself and use it as hidden neutron/layer when using RNN-package or how to extract neutron part from kohonen-package and 'merge' it with rnn-package. What I understood, this would require more in-depth studying of R-language and the packages earlier mentioned, and possibly writing completely own software from scratch using the specifications of recurrent neural networks and self-organizing maps/neutrons. 

I would proceed with this task by changing rnn-package to Tensorflow, as it is the de-facto industry accepted standard library for machine learning. The use of Tensorflow wouldn't restrict the developer to use only specific programming language because it has been ported to multiple environments. Tensorflow has more features, is better documented, has more examples and has more active developer&user base. From my understanding, tensorflow/keras allows different hidden layers to be used when doing RNN-learning, make
ing it possible to use Kohonen-neutron as neutrons in those hidden layers. This would still require more validation and studying. The development of the rnn-package seems to be halted as even the contributors are [switching to Tensorflow/Keras](https://github.com/bquast/rnn/issues/29#issuecomment-397663140).
