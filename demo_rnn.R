# Clear workspace
rm(list=ls())

# Load libraries
require(rnn)
library(tidyverse) # general utility functions
require(caret) # machine learning utility functions

# read in our data
weather_data <- read_csv("inputs/seattleWeather_1948-2017.csv")

# check out the first few rows
head(weather_data)

# set some parameters for our model
max_len <- 6 # the number of previous examples we'll look at
batch_size <- 32 # number of sequences to look at at one time during training
total_epochs <- 20 # how many times we'll look @ the whole dataset while training our model

# set a random seed for reproducability
set.seed(123)

# select out the colum with info on how often it rained
rain <- weather_data$RAIN

# summerize this
table(rain)

# Cut the text in overlapping sample sequences of max_len characters

# get a list of start indexes for our (overlapping) chunks
start_indexes <- seq(1, length(rain) - (max_len + 1), by = 3)

# create an empty matrix to store our data in
weather_matrix <- matrix(nrow = length(start_indexes), ncol = max_len + 1)

# fill our matrix with the overlapping slices of our dataset
for (i in 1:length(start_indexes)){
  weather_matrix[i,] <- rain[start_indexes[i]:(start_indexes[i] + max_len)]
}

# make sure it's numeric
weather_matrix <- weather_matrix * 1

# remove na's if you have them
if(anyNA(weather_matrix)){
    weather_matrix <- na.omit(weather_matrix)
}

# split our data into the day we're predict (y), and the
# sequence of days leading up to it (X)
X <- weather_matrix[,-ncol(weather_matrix)]
y <- weather_matrix[,ncol(weather_matrix)]

# print how the data looks at the moment
head(weather_matrix)
dim(weather_matrix)
head(y)
head(X)

# create an index to split our data into testing & training sets
training_index <- createDataPartition(y, p = .9,
                                  list = FALSE,
                                  times = 1)

#data should be in a form where 1st dimension is samples(should be same size in X and Y), 2st time(should be same size in X and Y), and 3rd optional variables dimension.
#
# samples = the number of observation this is the number of sample you have in your dataset, same as for classical training with other algorithm in R
# time = is the length in time of each input (e.g. the number of words in a sentence)."
# variable = the number of input/output unit.
# once more "the first dimension is for the number of samples, the second for the number of elements in the sequence and the optional third dimension for the number of variables"


# training data
#X_train <- array(cbind(X[training_index,], 0), dim = c(length(training_index), max_len+1, max_len))
X_train <- cbind(X[training_index,], 2)
#y_train <- array(weather_matrix[training_index,], dim = c(length(training_index), max_len+1))
y_train <- weather_matrix[training_index,]

# testing data
#X_test <- array(cbind(X[-training_index,],0), dim = c(length(y) - length(training_index), max_len+1))
#y_test <- array(weather_matrix[-training_index,], dim = c(length(y) - length(training_index), max_len+1))

#X_test <- array(cbind(X[-training_index,], 0), dim = c(length(y) - length(training_index), max_len+1, max_len))
X_test <- cbind(X[-training_index,],2)
y_test <- weather_matrix[-training_index,]

trained_model <- trainr(
    Y = y_train, # sequence we're predicting
    X = X_train, # sequence we're using for prediction
    hidden_dim = c(6), #hidden layer dimensions
    network_type = "rnn", #other types are gru and lstm, they are experimental in this package.
    sigmoid = c("logistic"),
    batch_size = batch_size, # how many samples to pass to our model at a time
    numepochs = total_epochs, # how many times we'll look @ the whole dataset
    learningrate = 0.1) # how much data to hold out for testing as we go along

pred_model <- predictr(trained_model, X_test) #predict if it is going to rain

error = colMeans(trained_model$error) #get error rate from predicted model
plot(error,type="l",xlab="epoch",ylab = "error") #create graph(plot) showing epoch error rate

#get just the rain column
test <- y_test[,7] 
pred_test <- round(pred_model[,7]) 

#create visualisations form the actual data and predicted data
table(test, pred_test)
hist(test- pred_test )
head((test -pred_test), 20)
confusionMatrix(table(test,pred_test))

# comparing to just previous day
day_before <- X_test[,max_len]
table(test, day_before)
